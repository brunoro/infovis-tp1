<?php

$countries = file("CountryCodes");
$conts = file("Continents");

$index = array();
foreach ($conts as $cont) {
  $a = explode(";", $cont);
  $index[trim($a[2])] = trim($a[0]);
}

$conts = array(
  "AFRICA"       => array(),
  "ASIA"         => array(),
  "EUROPE"       => array(),
  "NORTHAMERICA" => array(),
  "SOUTHAMERICA" => array(),
  "OCEANIA"      => array()
);

$labels = array(
  "AF" => "AFRICA",
  "AS" => "ASIA",
  "EU" => "EUROPE",
  "NA" => "NORTHAMERICA",
  "SA" => "SOUTHAMERICA",
  "OC" => "OCEANIA"
);

foreach ($countries as $country)
  $conts[$labels[$index[trim($country)]]][] = trim($country);

echo "var continents = " . json_encode($conts) . ";";